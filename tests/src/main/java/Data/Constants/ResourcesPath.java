package Data.Constants;

public class ResourcesPath {

    public final static String TEST_DATA_PATH = "src/main/resources/TestData/";
    public final static String ACCOUNT_SUMMARY_TEST_DATA_PATH = TEST_DATA_PATH + "AccountSummary/";

}
