package Data.DataProviders;

import Data.Enums.AccountModelTypeEnum;
import org.testng.annotations.DataProvider;

public class AccountSummaryDataProvider {

    @DataProvider(name = "accountsTableDataProviderJson")
    public static Object[][] populateAccountProviderWithJsonExpected() {
        return new Object[][]{
                {AccountModelTypeEnum.CASH, "expectedCashAccounts.json"},
                {AccountModelTypeEnum.INVESTMENT, "expectedInvestmentAccounts.json"},
                {AccountModelTypeEnum.CREDIT, "expectedCreditAccounts.json"},
                {AccountModelTypeEnum.LOAN, "expectedLoanAccounts.json"},
        };
    }


}
