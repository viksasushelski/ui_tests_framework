package Utils;

import Drivers.DriverFactory;
import Enums.DriverType;
import TestExecution.TestConfiguration;

public class BaseTest extends TestConfiguration {

    public final static int numberOfThreads = Integer.parseInt(PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH, MyPropertiesConstants.THREAD_COUNT_PROPERTY_NAME));
    public final static int numberOfReRunFailedTests = Integer.parseInt(PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH, MyPropertiesConstants.RE_RUN_FAILED_TESTS_PROPERTY_NAME));
    public final static String BASE_URL = PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH, MyPropertiesConstants.BASE_URL_PROPERTY_NAME);
    public final static String ENVIRONMENT = PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH, MyPropertiesConstants.ENVIRONMENT_PROPERTY_NAME);

    public final static String stepsToReproduceFilePath = "src/test/TestsStepsToReproduce/";
    public final static String stepsToReproduceConsoleLogFilePath = "src/test/TestsConsoleLog/";

    public static final String SUREFIRE_REPORTS_FOLDER_PATH = "target/surefire-reports/";
    public static final String TEST_OUTPUT_FOLDER_PATH = "test-output/";

    public BaseTest() {
        super(numberOfThreads, stepsToReproduceFilePath, stepsToReproduceConsoleLogFilePath, numberOfReRunFailedTests
                , SUREFIRE_REPORTS_FOLDER_PATH, TEST_OUTPUT_FOLDER_PATH);
    }

    @Override
    protected void initializeTest() {
        synchronized (this) {
            driver = DriverFactory.createDriverForBrowserType(DriverType.parse
                            (PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH,
                                    MyPropertiesConstants.DRIVER_TYPE_PROPERTY_NAME))
                    , PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH,
                            MyPropertiesConstants.DRIVER_VERSION_PROPERTY_NAME)
                    , Boolean.valueOf(PropertiesReader.readFromProperties(MyPropertiesConstants.MY_PROPERTIES_PATH,
                            MyPropertiesConstants.DRIVER_HEADLESS_PROPERTY_NAME)));
        }
        baseUrl = BASE_URL;
        environment = ENVIRONMENT;
    }
}
