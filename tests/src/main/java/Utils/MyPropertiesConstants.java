package Utils;

public class MyPropertiesConstants {

    public static final String MY_PROPERTIES_PATH = "my.properties";
    public static final String BASE_URL_PROPERTY_NAME = "base.url";
    public static final String ENVIRONMENT_PROPERTY_NAME = "environment.name";
    public static final String DRIVER_TYPE_PROPERTY_NAME = "driver.type";
    public static final String DRIVER_VERSION_PROPERTY_NAME = "driver.version";
    public static final String DRIVER_HEADLESS_PROPERTY_NAME = "driver.headless";
    public static final String THREAD_COUNT_PROPERTY_NAME = "thread.count";
    public static final String RE_RUN_FAILED_TESTS_PROPERTY_NAME = "re.run.failed.count";

}
