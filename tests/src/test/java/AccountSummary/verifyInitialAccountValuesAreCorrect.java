package AccountSummary;

import Data.Constants.ResourcesPath;
import Data.DataProviders.AccountSummaryDataProvider;
import Data.Enums.AccountModelTypeEnum;
import Json.JsonParser;
import Models.AccountSummary;
import Models.AssertionResult;
import Pages.AccountSummary.AccountSummaryPage;
import Pages.Login.LoginPage;
import Utils.BaseTest;
import Utils.DataReader;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class verifyInitialAccountValuesAreCorrect extends BaseTest {

    private LoginPage loginPage;
    private AccountSummaryPage accountSummaryPage;
    private DataReader dataReader;

    @Override
    protected void beforeClass() throws Exception {
        loginPage = (LoginPage) new LoginPage(driver)
                .navigateTo(baseUrl + "login.html");
        accountSummaryPage = loginPage.loginUsingCredentials("username", "password");
    }

    @Test(priority = 1, dataProvider = "accountsTableDataProviderJson", dataProviderClass = AccountSummaryDataProvider.class)
    public void checkAccountsContent(AccountModelTypeEnum accountModelTypeEnum, String expectedResultsJson) {
        dataReader = new JsonParser();
        accountSummaryPage = accountSummaryPage.getAccountModelTypeEnumReadFunctionMap().get(accountModelTypeEnum).get();
        List<AccountSummary> expectedAccountList = dataReader.parseFromFileToListOfObject
                (ResourcesPath.ACCOUNT_SUMMARY_TEST_DATA_PATH + expectedResultsJson, AccountSummary.class);
        AssertionResult assertionResult = accountSummaryPage.assertTableContent(accountModelTypeEnum, expectedAccountList);
        Assert.assertTrue(assertionResult.isPassed(), assertionResult.getErrorMessage());
    }
}
