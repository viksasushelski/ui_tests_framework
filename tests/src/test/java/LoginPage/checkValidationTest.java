package LoginPage;

import Log.Log;
import Models.AssertionResult;
import Pages.Login.LoginPage;
import Utils.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class checkValidationTest extends BaseTest {

    public static final String EXPECTED_VALIDATION_MESSAGE = "Login and/or password are wrong.";
    private LoginPage loginPage;

    @Override
    protected void beforeClass() throws Exception {
        loginPage = (LoginPage) new LoginPage(driver)
                .navigateTo("http://zero.webappsecurity.com/login.html");
        loginPage.insertUserName("fdsfds")
                .clickSubmit();
    }

    @Test
    private void checkSomething() {
        Log.assertion("Validation error message should be displayed with text: " + EXPECTED_VALIDATION_MESSAGE);
        AssertionResult assertionResult = loginPage.assertValidationMessageIsCorrect(EXPECTED_VALIDATION_MESSAGE);
        Assert.assertTrue(assertionResult.isPassed(), assertionResult.getErrorMessage());
    }

}
