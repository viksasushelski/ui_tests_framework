package AccountActivity;

import Data.Enums.AccountModelTypeEnum;
import Data.Enums.AccountTypeEnum;
import Pages.AccountActivity.AccountActivityPage;
import Pages.AccountSummary.AccountSummaryPage;
import Pages.Login.LoginPage;
import Utils.BaseTest;
import Utils.DataReader;
import org.testng.annotations.Test;

public class verifyInitialAccountTransactionValuesAreCorrect extends BaseTest {

    private LoginPage loginPage;
    private AccountSummaryPage accountSummaryPage;
    private AccountActivityPage accountActivityPage;
    private DataReader dataReader;

    @Override
    protected void beforeClass() throws Exception {
        loginPage = (LoginPage) new LoginPage(driver)
                .navigateTo(baseUrl + "login.html");
        accountSummaryPage = loginPage.loginUsingCredentials("username", "password");
        accountActivityPage = accountSummaryPage
                .readCreditAccounts()
                .openAccountActivityPage(AccountModelTypeEnum.CREDIT, "Checking");
    }

    @Test
    public void checkAccountsContent() {
        accountActivityPage = accountActivityPage
                .navigateToShowTransactionTab()
                .chooseAccount(AccountTypeEnum.LOAN)
                .readTransactionsTable();
        System.out.println("fdsfds");
    }
}
