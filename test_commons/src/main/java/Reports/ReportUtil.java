package Reports;

import Log.Log;
import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;


public class ReportUtil {
    private static String generateTestInfo(String environment, String screenshotPath) {
        return "\nEnvironment: " + environment +
                "\nFailed test screenshot path:\n" + screenshotPath;
    }

    public static void writeStepsToReproduceToFile(String text, String name) throws IOException {
        List<String> steps = Arrays.asList(text.split("\n"));
        Path file = Paths.get(name + ".txt");
        Files.write(file, steps, Charset.forName("UTF-8"));
    }

    public static void saveScreenShot(ITestResult result, File scrFile, String baseUrl, String pictureName) {

        pictureName = pictureName.replaceAll(":", "-");
        if (result.getStatus() != 1) {
            Log.info("\n Taking screenshots for failed test with name: " + result.getMethod().getTestClass().getName());
            File destFile = new File(pictureName);
            try {
                FileUtils.copyFile(scrFile, destFile);
            } catch (IOException e) {
                Log.error("Screenshot failed");
            }
            Object[] objects = new Object[1];
            objects[0] = ReportUtil.generateTestInfo(baseUrl, destFile.getAbsolutePath());
            result.setParameters(objects);
        } else {
            try {
                Path path = Paths.get(pictureName);
                Files.delete(path);
            } catch (NoSuchFileException x) {
                System.err.format("%s: no such" + " file or directory%n", pictureName);
            } catch (DirectoryNotEmptyException x) {
                System.err.format("%s not empty%n", pictureName);
            } catch (IOException x) {
                System.err.println(x);
            }
        }
    }
}
