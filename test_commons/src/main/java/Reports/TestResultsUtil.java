package Reports;

import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.*;
import java.util.stream.Collectors;

public class TestResultsUtil {


    public static Map<String, List<ITestNGMethod>> getTestNameWithCollectionOfTestMethodsMapFromFailedTestsInSuite(ITestContext context) {
        Map<String, ISuiteResult> results = context.getSuite().getResults();
        Map<String, List<ITestNGMethod>> testNameFailedMethodsMap = results.entrySet().stream().filter(stringISuiteResultEntry -> stringISuiteResultEntry.getValue().getTestContext().getFailedTests().size() > 0)
                .collect(Collectors.toMap(o -> o.getValue().getTestContext().getName(), o -> new ArrayList<>(o.getValue().getTestContext().getFailedTests().getAllMethods())));
        Map<String, List<ITestNGMethod>> testNameSkippedMethodsMap = results.entrySet().stream().filter(stringISuiteResultEntry -> stringISuiteResultEntry.getValue().getTestContext().getSkippedTests().size() > 0)
                .collect(Collectors.toMap(o -> o.getValue().getTestContext().getName(), o -> new ArrayList<>(o.getValue().getTestContext().getSkippedTests().getAllMethods())));
        Map<String, List<ITestNGMethod>> testNameFailedAndSkippedMethodsMap = new HashMap<>();
        testNameFailedAndSkippedMethodsMap.putAll(testNameFailedMethodsMap);
        for (Map.Entry<String, List<ITestNGMethod>> stringCollectionEntry : testNameSkippedMethodsMap.entrySet()) {
            if (testNameFailedAndSkippedMethodsMap.containsKey(stringCollectionEntry.getKey())) {
                testNameFailedAndSkippedMethodsMap.get(stringCollectionEntry.getKey()).addAll(stringCollectionEntry.getValue());
            } else {
                testNameFailedAndSkippedMethodsMap.put(stringCollectionEntry.getKey(), stringCollectionEntry.getValue());
            }
        }
        return testNameFailedAndSkippedMethodsMap;
    }

    public static Map<XmlTest, Set<XmlClass>> getTestNameWithSetOfXmlTestClassesMapFromFailedMethods(Map<String, List<ITestNGMethod>> testNameFailedAndSkippedMethodsMap) {
        List<ITestNGMethod> failedAndSkipMethods = testNameFailedAndSkippedMethodsMap.entrySet().stream().flatMap(stringCollectionEntry -> stringCollectionEntry.getValue().stream())
                .collect(Collectors.toList());
        return failedAndSkipMethods.stream().collect(Collectors.groupingBy(ITestNGMethod::getXmlTest,
                Collectors.mapping(o -> new XmlClass(o.getRealClass()), Collectors.toSet())));
    }

    public static XmlSuite createSuiteForReRunOfFailedTests(String suiteName, Map<XmlTest, Set<XmlClass>> testNameWithSetOfXmlTestClassesMapFromFailedMethods) {
        XmlSuite suite = new XmlSuite();
        suite.setName(suiteName);
        suite.setVerbose(1);
        for (Map.Entry<XmlTest, Set<XmlClass>> xmlTestXmlClassesMap : testNameWithSetOfXmlTestClassesMapFromFailedMethods.entrySet()) {
            XmlTest xmlTest = new XmlTest();
            xmlTest.setVerbose(1);
            xmlTest.setName(xmlTestXmlClassesMap.getKey().getName());
            Map<String, String> testParameters = xmlTestXmlClassesMap.getKey().getTestParameters();
            xmlTest.setParameters(testParameters);
            xmlTestXmlClassesMap.getValue();
            List<XmlClass> xmlClassList = new ArrayList<>();
            xmlClassList.addAll(xmlTestXmlClassesMap.getValue());
            xmlTest.setClasses(xmlClassList);
            xmlTest.setXmlSuite(suite);
            suite.addTest(xmlTest);
        }
        return suite;
    }

}
