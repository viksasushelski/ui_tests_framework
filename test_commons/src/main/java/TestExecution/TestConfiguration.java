package TestExecution;

import Log.Log;
import Reports.ReportUtil;
import Reports.TestResultsUtil;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public abstract class TestConfiguration {

    private static final String FAILED_TESTS = "Failed Tests";
    private static final String DEFAULT_SUITE = "Default Suite";
    public static final String TEST_CLASS_HAS_ENDED = "Test class has ended :";
    private boolean skipTestFlag = false;
    protected SoftAssert softAssert;
    protected String environment;
    protected String baseUrl;
    private File scrFile;
    private static int reRunCount;
    protected WebDriver driver;

    private Integer numberOfThreads;
    private String stepsToReproduceFilePath;
    private String stepsToReproduceConsoleLogFilePath;
    private Integer numberOfReRunFailedTests;
    private String initialFailedTestScreenShotPath;
    private String reRunFailedTestScreenShotPath;

    public TestConfiguration(Integer numberOfThreads, String stepsToReproduceFilePath, String stepsToReproduceConsoleLogFilePath, Integer numberOfReRunFailedTests
    ,String initialFailedTestScreenShotPath, String reRunFailedTestScreenShotPath) {
        this.numberOfThreads = numberOfThreads;
        this.stepsToReproduceFilePath = stepsToReproduceFilePath;
        this.stepsToReproduceConsoleLogFilePath = stepsToReproduceConsoleLogFilePath;
        this.numberOfReRunFailedTests = numberOfReRunFailedTests;
        this.initialFailedTestScreenShotPath = initialFailedTestScreenShotPath;
        this.reRunFailedTestScreenShotPath = reRunFailedTestScreenShotPath;
    }


    private void takeScreenShot() {
        scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    }

    @BeforeSuite(alwaysRun = true)
    protected void beforeSuite(ITestContext context) {
        String suiteName = context.getSuite().getName();
        Log.stepsToReproduce("Suite with name: " + suiteName + " has started");
        try {
            File sureFireSuite = new File(initialFailedTestScreenShotPath);
            File testNgSuite = new File(reRunFailedTestScreenShotPath);

            if (suiteName.equals("Review And Rate Tests")) {
                if (sureFireSuite.exists() && sureFireSuite.isDirectory()) {
                    org.apache.commons.io.FileUtils.cleanDirectory(sureFireSuite);
                }
                if (testNgSuite.exists() && testNgSuite.isDirectory()) {
                    org.apache.commons.io.FileUtils.cleanDirectory(testNgSuite);
                }
            } else {
                sureFireSuite = new File(initialFailedTestScreenShotPath + suiteName);
                testNgSuite = new File(reRunFailedTestScreenShotPath + suiteName);

                if (sureFireSuite.exists() && sureFireSuite.isDirectory()) {
                    org.apache.commons.io.FileUtils.cleanDirectory(sureFireSuite);
                }
                if (testNgSuite.exists() && testNgSuite.isDirectory()) {
                    org.apache.commons.io.FileUtils.cleanDirectory(testNgSuite);
                }
            }


        } catch (Exception e) {
            Log.info("There is no target or test-output file");
        }

        if (!suiteName.contains(FAILED_TESTS)) {
            reRunCount = 0;
            ISuite suite = context.getSuite();
            suite.getXmlSuite().setThreadCount(numberOfThreads);
            suite.getXmlSuite().setConfigFailurePolicy(XmlSuite.FailurePolicy.CONTINUE);
        }
    }

    protected abstract void initializeTest();

    @BeforeClass
    protected final void beforeClassImplementation() throws Exception {
        initializeTest();
        beforeClass();
        Log.stepsToReproduce("Before has ended for: " + this.getClass().getName());
    }

    protected void beforeClass() throws Exception {
    }

    @BeforeMethod()
    protected final void beforeMethodImplementation(Method method) {
        softAssert = new SoftAssert();
        Log.startTestCase(method.getName());
        this.beforeMethod();
    }

    protected void beforeMethod() {
    }

    @AfterMethod(alwaysRun = true)
    protected final void afterMethodImplementation(ITestResult result) {

        try {
            afterMethod();
        } catch (Exception e) {
            Log.error("The override after method failed due to exception");
        }
        takeScreenShot();
        String methodName = "Method";
        String pictureName = "picture";
        String picturePath;
        if (result != null && result.getStatus() != 1 && !skipTestFlag) {
            int status = result.getStatus();
            XmlSuite suite = result.getMethod().getXmlTest().getSuite();
            String suiteName = suite.getName();
            methodName = result.getMethod().getMethodName();
            if (status == -1) {
                pictureName = suiteName + "/" + this.getClass().getName() + "--Before";
                skipTestFlag = true;
            }
            if (status == 2) {
                pictureName = suiteName + "/" + this.getClass().getName() + "---" + methodName;
            }
            if (result.getMethod().getXmlTest().getSuite().getName().contains(FAILED_TESTS)) {
                picturePath = reRunFailedTestScreenShotPath + pictureName + ".png";
            } else {
                picturePath = initialFailedTestScreenShotPath + pictureName + ".png";
            }
            ReportUtil.saveScreenShot(result, scrFile, baseUrl, picturePath);
            Log.stepsToReproduce("Url when test: " + this.getClass().getName() + " failed: " + driver.getCurrentUrl());
        }
        Log.endTestCase(methodName);
    }

    protected void afterMethod() {
    }

    @AfterClass(alwaysRun = true)
    protected void afterClassImplementation(ITestContext context) throws Exception {
        afterClass();
        Log.stepsToReproduce("Test class has ended: " + this.getClass().getName());
        cleanUp(context);
    }

    protected void cleanUp(ITestContext context) throws IOException {
        try {
            String parallel = context.getSuite().getParallel();
            if (parallel.equals("none")) {
                ReportUtil.writeStepsToReproduceToFile(Log.getStepsLog().toString(),
                        stepsToReproduceFilePath + this.getClass().getName());
                Log.cleanTestSuitLog();
                ReportUtil.writeStepsToReproduceToFile(Log.getConsoleLog().toString(),
                        stepsToReproduceConsoleLogFilePath + this.getClass().getName());
                Log.cleanConsoleLog();
            }
        } finally {
            synchronized (this) {
                driver.close();
                driver.quit();
            }
            Log.stepsToReproduce(TEST_CLASS_HAS_ENDED + this.getClass().getName());
        }
    }

    protected void afterClass() {
    }

    @AfterSuite(alwaysRun = true)
    protected void afterSuite(ITestContext context) throws IOException {
        String suiteName = context.getSuite().getName();
        Log.stepsToReproduce("Suite with name: " + suiteName + " has ended");
        String parallel = context.getSuite().getParallel();
        if (parallel.equals("tests")) {
            if (!suiteName.equals("allSuites")) {
                suiteName = suiteName.replace(" ", "-");
                divideAndWriteInFileTestsStepsToReproduceForEachTestClassWhenInParallelMode(suiteName);
                divideAndWriteInFileConsoleLogForEachTestClassWhenInParallelMode(suiteName);
                Log.cleanTestSuitLog();
                Log.cleanConsoleLog();
            }
        }
        if (reRunCount < numberOfReRunFailedTests && !suiteName.equals(DEFAULT_SUITE)) {
            rerunFailedSuiteTests(suiteName, context);
        }
    }

    private void rerunFailedSuiteTests(String suiteName, ITestContext context) {
        Map<String, List<ITestNGMethod>> testNameWithCollectionOfTestMethodsMapFromFailedTestsInSuite = TestResultsUtil.getTestNameWithCollectionOfTestMethodsMapFromFailedTestsInSuite(context);
        if (testNameWithCollectionOfTestMethodsMapFromFailedTestsInSuite.size() > 0) {
            Map<XmlTest, Set<XmlClass>> testNameWithSetOfXmlTestClassesMapFromFailedMethods = TestResultsUtil.getTestNameWithSetOfXmlTestClassesMapFromFailedMethods(testNameWithCollectionOfTestMethodsMapFromFailedTestsInSuite);
            TestNG testNG = new TestNG();
            XmlSuite suite = TestResultsUtil.createSuiteForReRunOfFailedTests(suiteName + FAILED_TESTS, testNameWithSetOfXmlTestClassesMapFromFailedMethods);
            testNG.setXmlSuites(Collections.singletonList(suite));
            reRunCount++;
            testNG.run();
        }
    }

    private void divideAndWriteInFileTestsStepsToReproduceForEachTestClassWhenInParallelMode(String suiteName) throws IOException {
        String stepsToReproduce = Log.getStepsLog().toString();
        createTestStepsTextFiles(stepsToReproduce, suiteName, stepsToReproduceFilePath);
    }

    private void divideAndWriteInFileConsoleLogForEachTestClassWhenInParallelMode(String suiteName) throws IOException {
        String stepsToReproduce = Log.getConsoleLog().toString();
        createTestStepsTextFiles(stepsToReproduce, suiteName, stepsToReproduceConsoleLogFilePath);
    }

    private void createTestStepsTextFiles(String log, String suiteName, String filePath) throws IOException {
        log = log.replaceAll("(?m)^[ \t]*\r?\n", "");
        List<String> steps = Arrays.asList(log.split("\n"));
        List<String> removedLinesWithOutThreadID = steps.stream().filter(s -> s.contains("ThreadID")).collect(Collectors.toList());
        Map<Integer, List<String>> threadIdStepsMap = removedLinesWithOutThreadID.stream().collect(Collectors.groupingBy(this::getThreadId));
        for (Map.Entry<Integer, List<String>> threadSteps : threadIdStepsMap.entrySet()) {
            StringBuilder stepsBuilder = new StringBuilder();
            for (String s : threadSteps.getValue()) {
                s = s.trim() + "\n";
                stepsBuilder.append(s);
                if (s.contains(TEST_CLASS_HAS_ENDED)) {
                    String[] split = s.split(":");
                    ReportUtil.writeStepsToReproduceToFile(stepsBuilder.toString(), filePath + suiteName + "--" + split[split.length - 1].trim());
                    stepsBuilder = new StringBuilder();
                }
            }
        }
    }

    private int getThreadId(String line) {
        List<String> words = Arrays.asList(line.split("--"));
            return Integer.parseInt(words.get(1).split(":")[1]);
    }

}
