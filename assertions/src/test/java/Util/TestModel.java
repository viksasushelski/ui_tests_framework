package Util;

import Utils.CompareDifferences;
import lombok.Data;

@Data
public class TestModel implements CompareDifferences {

    private String text;
    private Boolean complexFlag;
    private boolean flag;
    private Integer complexNum;
    private int num;
    private Long complexLongNum;
    private long longNum;
    private Double complexDoubleNum;
    private double doubleNum;

}
