import ListAssertions.ListAssertor;
import ListAssertions.ListAssertorBuilder;
import Models.AssertionResult;
import Util.TestModel;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ListAssertorTest {

    @Test
    public void checkSortingByOneAttributeIsWorking() {
        //Arrange
        TestModel firstTestModel = new TestModel();
        firstTestModel.setText("text1");
        firstTestModel.setNum(2);
        firstTestModel.setLongNum(2L);

        TestModel secondTestModel = new TestModel();
        secondTestModel.setText("text2");
        secondTestModel.setNum(2);
        secondTestModel.setLongNum(2L);

        List<TestModel> firstList = Arrays.asList(firstTestModel, secondTestModel);
        List<TestModel> secondList = Arrays.asList(secondTestModel, firstTestModel);

        //Act
        ListAssertor<TestModel> listAssertor = new ListAssertorBuilder<TestModel>()
                .setActualList(firstList)
                .setExpectedList(secondList)
                .setComparator(Comparator.comparing(TestModel::getText))
                .setEqualsGetterFunctions(Arrays.asList(TestModel::getText, TestModel::getNum, TestModel::getLongNum))
                .setClassesToIgnore(new ArrayList<>())
                .build();
        //Assert
        AssertionResult assertionResult = listAssertor.checkIfListsAreEqual();
        Assert.assertTrue(assertionResult.getErrorMessage(), assertionResult.isPassed());
    }

    @Test
    public void checkSortingByMultipleAttributeIsWorking() {
        //Arrange
        TestModel firstTestModel = new TestModel();
        firstTestModel.setText("text1");
        firstTestModel.setNum(2);
        firstTestModel.setLongNum(2L);

        TestModel secondTestModel = new TestModel();
        secondTestModel.setText("text2");
        secondTestModel.setNum(2);
        secondTestModel.setLongNum(2L);

        List<TestModel> firstList = Arrays.asList(firstTestModel, secondTestModel);
        List<TestModel> secondList = Arrays.asList(secondTestModel, firstTestModel);

        //Act
        ListAssertor<TestModel> listAssertor = new ListAssertorBuilder<TestModel>()
                .setActualList(firstList)
                .setExpectedList(secondList)
                .setComparator(Comparator.comparing(TestModel::getNum).thenComparing(TestModel::getText))
                .setEqualsGetterFunctions(Arrays.asList(TestModel::getText, TestModel::getNum, TestModel::getLongNum))
                .setClassesToIgnore(new ArrayList<>())
                .build();
        //Assert
        AssertionResult assertionResult = listAssertor.checkIfListsAreEqual();
        Assert.assertTrue(assertionResult.getErrorMessage(), assertionResult.isPassed());
    }

    @Test
    public void checkEqualsGetterFunctionsAreWorkingCase1() {
        //Arrange
        TestModel firstTestModel = new TestModel();
        firstTestModel.setText("text1");
        firstTestModel.setNum(2);
        firstTestModel.setLongNum(2L);

        TestModel secondTestModel = new TestModel();
        secondTestModel.setText("text2");
        secondTestModel.setNum(2);
        secondTestModel.setLongNum(2L);

        List<TestModel> firstList = Collections.singletonList(firstTestModel);
        List<TestModel> secondList = Collections.singletonList(secondTestModel);

        //Act
        ListAssertor<TestModel> listAssertor = new ListAssertorBuilder<TestModel>()
                .setActualList(firstList)
                .setExpectedList(secondList)
                .setComparator(Comparator.comparing(TestModel::getText))
                .setEqualsGetterFunctions(Arrays.asList(TestModel::getNum, TestModel::getLongNum))
                .setClassesToIgnore(new ArrayList<>())
                .build();
        //Assert
        AssertionResult assertionResult = listAssertor.checkIfListsAreEqual();
        Assert.assertTrue(assertionResult.getErrorMessage(), assertionResult.isPassed());
    }

    @Test
    public void checkEqualsGetterFunctionsAreWorkingCase2() {
        //Arrange
        TestModel firstTestModel = new TestModel();
        firstTestModel.setText("text1");
        firstTestModel.setNum(2);
        firstTestModel.setLongNum(2L);

        TestModel secondTestModel = new TestModel();
        secondTestModel.setText("text2");
        secondTestModel.setNum(2);
        secondTestModel.setLongNum(2L);

        List<TestModel> firstList = Collections.singletonList(firstTestModel);
        List<TestModel> secondList = Collections.singletonList(secondTestModel);

        //Act
        ListAssertor<TestModel> listAssertor = new ListAssertorBuilder<TestModel>()
                .setActualList(firstList)
                .setExpectedList(secondList)
                .setComparator(Comparator.comparing(TestModel::getText))
                .setEqualsGetterFunctions(Arrays.asList(TestModel::getText, TestModel::getNum, TestModel::getLongNum))
                .setClassesToIgnore(new ArrayList<>())
                .build();
        //Assert
        AssertionResult assertionResult = listAssertor.checkIfListsAreEqual();
        Assert.assertFalse(assertionResult.getErrorMessage(), assertionResult.isPassed());
    }

    @Test
    public void checkGeneratedErrorMessage() {
        //Arrange
        TestModel firstTestModel = new TestModel();
        firstTestModel.setText("text1");
        firstTestModel.setNum(2);
        firstTestModel.setLongNum(2L);

        TestModel secondTestModel = new TestModel();
        secondTestModel.setText("text2");
        secondTestModel.setNum(3);
        secondTestModel.setLongNum(2L);

        List<TestModel> firstList = Collections.singletonList(firstTestModel);
        List<TestModel> secondList = Collections.singletonList(secondTestModel);

        //Act
        ListAssertor<TestModel> listAssertor = new ListAssertorBuilder<TestModel>()
                .setActualList(firstList)
                .setExpectedList(secondList)
                .setComparator(Comparator.comparing(TestModel::getText))
                .setEqualsGetterFunctions(Arrays.asList(TestModel::getText, TestModel::getNum, TestModel::getLongNum))
                .setClassesToIgnore(Collections.singletonList(String.class))
                .build();
        //Assert
        AssertionResult assertionResult = listAssertor.checkIfListsAreEqual();
        String errorMessage = assertionResult.getErrorMessage();
        Assert.assertEquals("There is difference for this object : TestModel(text=text1, complexFlag=null, flag=false, complexNum=null, num=2, complexLongNum=null, longNum=2, complexDoubleNum=null, doubleNum=0.0)\n" +
                "num is different. It is expected to be: 3".trim(), errorMessage.trim());
    }

}
