package Utils;


import Models.AssertionResult;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestAssertionUtil {

    public static AssertionResult checkIfListsAreEqual(List<? extends CompareDifferences> actual, List<? extends CompareDifferences> expected, Class<?>... classesToIgnore) {
        if (actual.size() != expected.size()) {
            return AssertionResult.builder()
                    .passed(false)
                    .errorMessage("There is difference in lists size")
                    .build();
        }
        actual = sortListByComparator(actual, classesToIgnore);
        expected = sortListByComparator(expected);

        StringBuilder errorMessage = new StringBuilder();
        for (int i = 0; i < expected.size(); i++) {
            if (!actual.get(i).equals(expected.get(i))) {
                errorMessage.append(expected.get(i).compareAndPrintAllDifferences(actual.get(i), classesToIgnore))
                        .append("\n");
            }
        }
        return AssertionResult.builder()
                .passed(errorMessage.toString().equals(""))
                .errorMessage(errorMessage.toString())
                .build();
    }


    public static AssertionResult checkIfListsOfStringsAreEqual(List<String> actual, List<String> expected) {
        if (actual.size() != expected.size()) {
            return AssertionResult.builder()
                    .passed(false)
                    .errorMessage("There is difference in lists size")
                    .build();
        }
        actual = sortListByComparator(actual, Comparator.comparing(String::toString));
        expected = sortListByComparator(expected, Comparator.comparing(String::toString));

        StringBuilder errorMessage = new StringBuilder();
        for (int i = 0; i < expected.size(); i++) {
            if (!actual.get(i).equals(expected.get(i))) {
                errorMessage.append("The value ")
                        .append(actual.get(i))
                        .append(" is not correct. It is expected to be: ")
                        .append(expected.get(i)).append("\n");
            }
        }
        return AssertionResult.builder()
                .passed(errorMessage.toString().equals(""))
                .errorMessage(errorMessage.toString())
                .build();
    }

    public static <T> boolean compareTwoObjectByFields(T object1, T object2, List<Function<T, Object>> functions) {
        if (object1 == object2) return true;
        if (object1 == null || object2 == null || object1.getClass() != object2.getClass()) return false;
        for (Function<T, Object> function : functions) {
            Object getter1 = function.apply(object1);
            Object getter2 = function.apply(object2);
            if (!(getter1 != null ? getter1.equals(getter2) : getter2 == null)) return false;
        }
        return true;
    }

    public static <T> boolean compareTwoObjectByFields(T object1, T object2, Function<T, Object>... functions) {
        return compareTwoObjectByFields(object1, object2, functions);
    }

    public static <T> List<T> sortListByComparator(List<T> unSortedList, Comparator<T> comparator) {
        return unSortedList.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }


    public static <T extends CompareDifferences> List<T> sortListByComparator(List<T> unSortedList, Class<?>... classesToIgnore) {
        return unSortedList.stream()
                .sorted((o1, o2) -> o1.compare(o2, classesToIgnore))
                .collect(Collectors.toList());
    }

}
