package Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public interface CompareDifferences {

    String DIFFERENCE_MESSAGE = " is different. It is expected to be: ";

    default String compareAndPrintAllDifferences(CompareDifferences actual, Class<?>... classesToIgnore) {
        StringBuilder builder = new StringBuilder();
        ObjectMapper objectMapper = new ObjectMapper();
        builder.append("There is difference for this object : ").append(actual.toString()).append("\n");
        List<Field> fieldsWithOutClassesToIgnore = removeClassesToIgnore(actual, Arrays.asList(classesToIgnore));
        for (Field declaredField : fieldsWithOutClassesToIgnore) {

            declaredField.setAccessible(true);
            try {
                Object expectedValue = declaredField.get(this);
                Object actualValue = declaredField.get(actual);
                String field1 = objectMapper.writeValueAsString(expectedValue);
                String field2 = objectMapper.writeValueAsString(actualValue);
                if (!field1.equals(field2)) {
                    builder.append(declaredField.getName()).append(DIFFERENCE_MESSAGE).append(expectedValue).append("\n");
                }

            } catch (IllegalAccessException | JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return builder.toString();
    }

    default int compare(CompareDifferences actual, Class<?>... classesToIgnore) {
        List<Field> fieldsWithOutWebElements = removeClassesToIgnore(actual, Arrays.asList(classesToIgnore));
        Comparator comparator = Comparator.comparing((Object o) -> {
            try {
                return (Comparable) fieldsWithOutWebElements.get(0).get(o);
            } catch (Exception e) {
                throw new RuntimeException("Comparator problems", e);
            }
        });
        for (int i = 1; i < fieldsWithOutWebElements.size(); i++) {
            int j = i;
            comparator = comparator.thenComparing((Object o) -> {
                        try {
                            return fieldsWithOutWebElements.get(j).get(o);
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException("Comparator problems", e);
                        }
                    }
            );
        }
        return comparator.compare(this, actual);
    }

    default List<Field> removeClassesToIgnore(CompareDifferences actual, List<Class<?>> classesToIgnore) {
        Field[] declaredFields = this.getClass().getDeclaredFields();
        List<Field> fields = Arrays.asList(declaredFields);
        List<Field> fieldsWithOutWebElements = fields.stream()
                .filter(field -> {
                    try {
                        field.setAccessible(true);
                        if(field.get(this)!=null){
                            return !( classesToIgnore.contains(field.get(this).getClass()) || field.get(this) instanceof ArrayList) && field.get(this) != null && field.get(actual) != null;
                        }else {
                            return false;
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .collect(Collectors.toList());
        return fieldsWithOutWebElements;
    }

}