package MapAssertions;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class MapAsserterBuilder<T, K> {
    private Map<T, K> actualMap;
    private Map<T, K> expectedMap;
    private List<Function<K, Object>> equalsGetterFunctionsForMapValuesForKeys;

    public MapAsserterBuilder<T, K> setActualMap(Map<T, K> actualMap) {
        this.actualMap = actualMap;
        return this;
    }

    public MapAsserterBuilder<T, K> setExpectedMap(Map<T, K> expectedMap) {
        this.expectedMap = expectedMap;
        return this;
    }

    public MapAsserterBuilder<T, K> setEqualsGetterFunctionsForMapValuesForKeys(List<Function<K, Object>> equalsGetterFunctionsForMapValuesForKeys) {
        this.equalsGetterFunctionsForMapValuesForKeys = equalsGetterFunctionsForMapValuesForKeys;
        return this;
    }

    public MapAssertor<T, K> build() {
        if (actualMap == null || expectedMap == null || equalsGetterFunctionsForMapValuesForKeys == null) {
            throw new RuntimeException("The List Asseter has null values");
        }
        return new MapAssertor<>(actualMap, expectedMap, equalsGetterFunctionsForMapValuesForKeys);
    }
}