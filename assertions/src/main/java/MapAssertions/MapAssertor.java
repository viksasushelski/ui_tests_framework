package MapAssertions;

import Models.AssertionResult;
import Utils.TestAssertionUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Getter
@Setter
public class MapAssertor<T, K> {

    private Map<T, K> actualMap;
    private Map<T, K> expectedMap;
    private List<Function<K, Object>> equalsGetterFunctionsForMapValues;

    private MapAssertor() {
    }

    public MapAssertor(Map<T, K> actualMap, Map<T, K> expectedMap,
                       List<Function<K, Object>> equalsGetterFunctionequalsGetterFunctionsForMapValuessForKeys) {
        this.actualMap = actualMap;
        this.expectedMap = expectedMap;
        this.equalsGetterFunctionsForMapValues = equalsGetterFunctionequalsGetterFunctionsForMapValuessForKeys;
    }

    public AssertionResult checkIfMapsAreEqual() {
        AssertionResult assertionResult = new AssertionResult(true, null);
        StringBuilder errorMessageBuilder = new StringBuilder();
        for (T t : expectedMap.keySet()) {
            if (!TestAssertionUtil.compareTwoObjectByFields(actualMap.get(t), expectedMap.get(t), equalsGetterFunctionsForMapValues)) {
                errorMessageBuilder.append("There is difference for : ").append(t.toString())
                        .append("---").append(expectedMap.get(t)).append("\n");
            }
        }

        if (!errorMessageBuilder.toString().isEmpty()) {
            assertionResult.setPassed(false);
        }
        assertionResult.setErrorMessage(errorMessageBuilder.toString());
        return assertionResult;
    }

}
