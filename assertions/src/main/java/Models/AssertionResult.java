package Models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AssertionResult {

    private boolean passed;
    private String errorMessage;

}
