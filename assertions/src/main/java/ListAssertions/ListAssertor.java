package ListAssertions;

import Models.AssertionResult;
import Utils.CompareDifferences;
import Utils.TestAssertionUtil;
import lombok.Getter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

@Getter
public class ListAssertor<T extends CompareDifferences> {

    private List<T> actualList;
    private List<T> expectedList;
    private List<Class<?>> classesToIgnore;
    private Comparator<T> comparator;
    private List<Function<T, Object>> equalsGetterFunctions;

    private ListAssertor() {
    }

    public ListAssertor(List<T> actualList, List<T> expectedList, Comparator<T> comparator, List<Function<T, Object>> equalsGetterFunctions,
                        List<Class<?>> classesToIgnore) {
        this.actualList = actualList;
        this.expectedList = expectedList;
        this.comparator = comparator;
        this.equalsGetterFunctions = equalsGetterFunctions;
        this.classesToIgnore = classesToIgnore;
    }

    public AssertionResult checkIfListsAreEqual() {
        AssertionResult assertionResult = new AssertionResult(true, null);
        StringBuilder errorMessageBuilder = new StringBuilder();
        List<T> actualSorted = TestAssertionUtil.sortListByComparator(actualList, comparator);
        List<T> expectedSorted = TestAssertionUtil.sortListByComparator(expectedList, comparator);
        for (int i = 0; i < expectedSorted.size(); i++) {
            if (!TestAssertionUtil.compareTwoObjectByFields(actualSorted.get(i), expectedSorted.get(i), equalsGetterFunctions)) {
                errorMessageBuilder.append(expectedSorted.get(i).compareAndPrintAllDifferences(actualSorted.get(i), classesToIgnore.toArray(new Class<?>[classesToIgnore.size()])));
            }
        }
        if (!errorMessageBuilder.toString().isEmpty()) {
            assertionResult.setPassed(false);
        }
        assertionResult.setErrorMessage(errorMessageBuilder.toString());
        return assertionResult;
    }

}
