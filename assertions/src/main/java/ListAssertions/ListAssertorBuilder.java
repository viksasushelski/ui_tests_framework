package ListAssertions;

import Utils.CompareDifferences;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class ListAssertorBuilder<T extends CompareDifferences> {
    private List<T> actualList;
    private List<T> expectedList;
    private List<Class<?>> classesToIgnore;
    private Comparator<T> comparator;
    private List<Function<T, Object>> equalsGetterFunctions;

    public ListAssertorBuilder<T> setActualList(List<T> actualList) {
        this.actualList = actualList;
        return this;
    }

    public ListAssertorBuilder<T> setExpectedList(List<T> expectedList) {
        this.expectedList = expectedList;
        return this;
    }

    public ListAssertorBuilder<T> setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
        return this;
    }

    public ListAssertorBuilder<T> setEqualsGetterFunctions(List<Function<T, Object>> equalsGetterFunctions) {
        this.equalsGetterFunctions = equalsGetterFunctions;
        return this;
    }

    public ListAssertorBuilder<T> setClassesToIgnore(List<Class<?>> classesToIgnore) {
        this.classesToIgnore = classesToIgnore;
        return this;
    }

    public ListAssertor<T> build() {
        if (actualList == null || expectedList == null || comparator == null || equalsGetterFunctions == null || classesToIgnore == null) {
            throw new RuntimeException("The List Asseter has null values");
        }
        return new ListAssertor<>(actualList, expectedList, comparator, equalsGetterFunctions, classesToIgnore);
    }
}