package Log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

public class Log {
    private static Logger Log = LoggerFactory.getLogger(Log.class.getName());//
    private static StringBuilder stepsLog = new StringBuilder();
    private static StringBuilder consoleLog = new StringBuilder();
    private static final String LOG_MESSAGE_FORMAT = "{timestamp}--ThreadID:{threadId}--{LogType}: {message}";

    private static String formatLogMessage(String logType, String message) {
        return LOG_MESSAGE_FORMAT.replace("{timestamp}", getTimeStamp().toString())
                .replace("{threadId}", getThreadId().toString())
                .replace("{LogType}", logType)
                .replace("{message}", message);
    }

    public static void startTestCase(String sTestCaseName) {
        stepsLog.append("\n").append(formatLogMessage("Steps", " : Test with name: " + sTestCaseName + " has started"));
        consoleLog.append("\n").append(formatLogMessage("Console", " : Test with name: " + sTestCaseName + " has started"));
    }

    public static void startTestCaseJira(String sJiraName, String sJiraNumber) {
        Log.info("Executing " + sJiraName + " ; " + sJiraNumber);
    }

    //This is to print log for the ending of the test case

    public static void endTestCase(String sTestCaseName) {
        stepsLog.append("\n").append(formatLogMessage("Steps", " : Test with name: " + sTestCaseName + " has ended"));
        consoleLog.append("\n").append(formatLogMessage("Console", " : Test with name: " + sTestCaseName + " has ended"));
    }

    // Need to create these methods, so that they can be called

    public static void info(String message) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long threadId = Thread.currentThread().getId();
        Log.info("\n" + timestamp + " --ThreadID : " + threadId + " : " + message);
    }

    public static void warn(String message) {
        Log.warn(message);
    }

    public static void error(String message) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long threadId = Thread.currentThread().getId();
        Log.info("\n" + timestamp + " --ThreadID : " + threadId + " : " + message);
        Log.error("\n" + timestamp + " --ThreadID : " + threadId + " : " + message);
    }


    static void debug(String message) {
        Log.debug(formatLogMessage("Debug", message));
    }

    public static void stepsToReproduce(String message) {
        Log.info(formatLogMessage("Info", message));
        stepsLog.append("\n").append(formatLogMessage("Steps", message));
        consoleLog.append("\n").append(formatLogMessage("Console", message));
    }

    public static void assertion(String message) {
        Log.info(formatLogMessage("Assertion", message));
        stepsLog.append("\n").append(formatLogMessage("Assertion", message));
    }

    public static void writeConsoleLog(String message) {
        Log.info(formatLogMessage("Console", message));
        consoleLog.append("\n").append(formatLogMessage("Console", message));
    }

    public static void waitingTime(long waitTime) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long threadId = Thread.currentThread().getId();
        stepsLog.append("\n" + timestamp + " --ThreadID :  " + threadId + " : Loading bar is waiting for :" + waitTime + "milliSeconds");
        consoleLog.append("\n" + timestamp + " --ThreadID :  " + threadId + " : Loading bar is waiting for :" + waitTime + "milliSeconds");
    }

    public static StringBuilder getStepsLog() {
        return stepsLog;
    }

    public static StringBuilder getConsoleLog() {
        return consoleLog;
    }

    public static void cleanTestSuitLog() {
        stepsLog = new StringBuilder();
    }

    public static void cleanConsoleLog() {
        consoleLog = new StringBuilder();
    }

    private static Long getThreadId() {
        return Thread.currentThread().getId();
    }

    private static Timestamp getTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }


}
