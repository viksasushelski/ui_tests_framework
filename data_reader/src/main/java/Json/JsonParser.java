package Json;

import Utils.DataReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonParser implements DataReader {

    static ObjectMapper objectMapper;
    static {
        objectMapper = new ObjectMapper();
    }

    public <E> List<E> parseJsonFromFileToListOfObject(String path, Class<?> typeClass) {
        try {
            List<E> results = objectMapper.readValue(new File(path),
                    TypeFactory.defaultInstance().constructCollectionType(List.class, typeClass));
            return results;
        } catch (IOException ex) {
//            log.error("Parsing json from file to list of objects failed for file: " + path);
            throw new RuntimeException(ex);
        }
    }

    public static Object parseJsonStringToObject(String jsonString, Class<?> typeClass) {
        try {
            Object result = objectMapper.readValue(jsonString,
                    typeClass);
            return result;
        } catch (IOException ex) {
//            log.error("Parsing json to list of objects failed for file: " + jsonString);
            throw new RuntimeException(ex);
        }
    }

    public static Object parseJsonToObject(String path, Class<?> typeClass) {
        try {
            Object result = objectMapper.readValue(new File(path), typeClass);
            return result;
        } catch (IOException ex) {
//            log.error("Parsing json to object failed for file: " + path);
            throw new RuntimeException(ex);
        }
    }

    public static String getJsonStringFromListOfObjects(List<?> objectList) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(objectList);
            return json;
        } catch (IOException ex) {
//            log.error("Parsing list to json failed");
            throw new RuntimeException(ex);
        }
    }

    public static String getJsonStringFromObject(Object object) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(object);
            return json;
        } catch (IOException ex) {
//            log.error("Parsing object to json failed");
            throw new RuntimeException(ex);
        }
    }


    @Override
    public <T> List<T> parseFromFileToListOfObject(String path, Class<?> typeClass) {
        return parseJsonFromFileToListOfObject(path, typeClass);
    }

    public <T> T parseFromFileToSingleObject(String path, Class<?> typeClass) {
        return parseFromFileToSingleObject(path, typeClass);
    }

}
