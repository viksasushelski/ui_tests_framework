package Excel;

import Utils.DataReader;

import java.util.ArrayList;
import java.util.List;

public abstract class ExcelParser implements DataReader {

    protected Excel excel;
    protected String sheetName;
    protected int startRow;
    protected int endRow;

    private ExcelParser() {
    }

    public ExcelParser(String sheetName) {
        this.sheetName = sheetName;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E> List<E> parseFromFileToListOfObject(String path, Class<?> typeClass) {
        excel = new Excel(path, sheetName);
        List<E> results = new ArrayList<>();
        for (int i = startRow; i < endRow; i++) {
            results.add(readFromExcelRowIntoModel());
        }
        return results;
    }

    public abstract <E> E readFromExcelRowIntoModel();
}
