package Utils;

import java.util.List;

public interface DataReader {

    <T> List<T> parseFromFileToListOfObject(String path, Class<?> typeClass);

//    <T> T parseFromFileToSingleObject(String path, Class<?> typeClass);

}
