package Components.Tables;

import Features.Tables.TableReader;
import lombok.Getter;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class BaseTableComponent {

    private WebElement tableComponent;
    private TableReader tableReader;
    private List<?> listOfEntries;

    public BaseTableComponent(WebElement tableComponent, TableReader tableReader) {
        this.tableComponent = tableComponent;
        this.tableReader = tableReader;
        listOfEntries = new ArrayList<>();
    }

    public void fillListOfEntires() {
        listOfEntries = readTableEntries();
    }

    private List<?> readTableEntries() {
        return tableReader.readTable(tableComponent);
    }

}
