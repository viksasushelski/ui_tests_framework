package Components.DropDownLists;

import Features.DropDownLists.DDLService;
import org.openqa.selenium.WebElement;

public abstract class BaseDDLComponent {

    private WebElement ddlComponent;
    private DDLService ddlService;

    public BaseDDLComponent(WebElement ddlComponent, DDLService ddlService) {
        this.ddlComponent = ddlComponent;
        this.ddlService = ddlService;
    }

    public void chooseFromDDL(Enum<?>... optionEnum) {
        ddlService.chooseItemFromDDL(ddlComponent, optionEnum);
    }

    public String getSelectedItem() {
        return ddlService.getSelectedItem(ddlComponent);
    }
    
}
