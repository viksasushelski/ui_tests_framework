package Enums;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum DriverType {
    GOOGLE_CHROME("Google Chrome"),
    FIREFOX("Firefox");

    private final String text;

    DriverType(final String text) {
        this.text = text;
    }

    private static Map<String, DriverType> values = populateMap();

    private static Map<String, DriverType> populateMap() {
        Map<String, DriverType> valuesMap = new HashMap<>();
        DriverType[] values = values();
        Stream.of(values).forEach(x -> valuesMap.put(x.text, x));
        return valuesMap;
    }

    @Override
    public String toString() {
        return text;
    }

    public static DriverType parse(String text) {
        if (!values.containsKey(text)) {
            throw new IllegalArgumentException("text");
        }

        return values.get(text);
    }
}
