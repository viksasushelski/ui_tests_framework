package Exceptions;

import java.util.function.Supplier;

/**
 * Created by vsushelski on 8/21/2017.
 */
public class DriverException extends RuntimeException implements Supplier<DriverException> {
    public DriverException() {
    }

    public DriverException(String message) {
        super(message);
    }

    @Override
    public DriverException get() {
        return null;
    }
}
