package Drivers.Implementations;

import Drivers.BaseDriver;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FireFoxDriverImplementation extends BaseDriver {
    @Override
    protected void setDriverVersion(String version) {
        FirefoxDriverManager.getInstance().version(version).setup();
    }

    @Override
    protected WebDriver createDriverAndSetOptions(boolean headless) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        if (headless) {
           //todo set firefox options
        }
        return new FirefoxDriver(firefoxOptions);
    }
}
