package Drivers.Implementations;

import Drivers.BaseDriver;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeDriverImplementation extends BaseDriver {
    @Override
    protected void setDriverVersion(String version) {
        ChromeDriverManager.getInstance().version(version).setup();
    }

    @Override
    protected WebDriver createDriverAndSetOptions(boolean headless) {

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setAcceptInsecureCerts(true);
        ChromeOptions chromeOptions = new ChromeOptions();
        if (headless) {
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("window-size=1920x1080");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--allow-insecure-localhost");
        }
        chromeOptions.merge(desiredCapabilities);
        return new ChromeDriver(chromeOptions);
    }
}
