package Drivers;

import Drivers.Implementations.ChromeDriverImplementation;
import Drivers.Implementations.FireFoxDriverImplementation;
import Enums.DriverType;
import Exceptions.DriverException;
import org.openqa.selenium.WebDriver;


public class DriverFactory {

    public static WebDriver createDriverForBrowserType(DriverType driverType, String driverVersion, boolean headless) {

        BaseDriverFunctions driver = null;

        if (driverType == DriverType.GOOGLE_CHROME) {
            driver = new ChromeDriverImplementation();
        }
        if (driverType == DriverType.FIREFOX) {
            driver = new FireFoxDriverImplementation();
        }
        if (driver == null) {
            throw new DriverException("There is implementation for this driver type:" + driverType.toString());
        }

        return driver.createWebDriver(driverVersion, headless);
    }


}
