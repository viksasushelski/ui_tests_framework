package Drivers;

import org.openqa.selenium.WebDriver;

public abstract class BaseDriver implements BaseDriverFunctions {

    @Override
    public WebDriver createWebDriver(String version, boolean headless) {
        setDriverVersion(version);
        WebDriver driver = createDriverAndSetOptions(headless);
        driver.manage().window().maximize();
        return driver;
    }

    protected abstract void setDriverVersion(String version);

    protected abstract WebDriver createDriverAndSetOptions(boolean headless);

}
