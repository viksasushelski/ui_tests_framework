package Drivers;

import org.openqa.selenium.WebDriver;

public interface BaseDriverFunctions {

    WebDriver createWebDriver(String version, boolean headless);
}
