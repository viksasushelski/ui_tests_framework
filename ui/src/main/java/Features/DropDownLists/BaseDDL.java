package Features.DropDownLists;

import UiFunctions.SeleniumFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseDDL extends SeleniumFunctions implements DDLService {

    private Boolean ddlShouldBeClosedManually;
    private Boolean unSelectFirstAllItems;

    public BaseDDL(WebDriver driver, int maxWaitingTimeForElements, Boolean ddlShouldBeClosedManually, Boolean unSelectFirstAllItems) {
        super(driver, maxWaitingTimeForElements);
        this.ddlShouldBeClosedManually = ddlShouldBeClosedManually;
        this.unSelectFirstAllItems = unSelectFirstAllItems;
    }

    @Override
    public void chooseItemFromDDL(WebElement ddlContainer, Enum<?>... optionEnum) {
        List<String> enumsTextValues = Arrays.asList(optionEnum).stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
        WebElement ddlButtonFromContainer = getDDLButtonFromContainer(ddlContainer);
        waitForElementToBeClickableAndClick(ddlButtonFromContainer);
        WebElement ddlElement = getDDLElementFromContainer(ddlContainer);
        List<WebElement> ddlOptions = findOptionRowsFromDDL(ddlElement);
        if (unSelectFirstAllItems) {
            for (WebElement element : ddlOptions) {
                if (checkIfOptionIsSelected(element)) {
                    waitForElementToBeClickableAndClick(getElementToClickFromOption(element));
                }
            }
        }
        List<WebElement> elementsToBeSelected = ddlOptions.stream()
                .filter(option -> enumsTextValues.contains(getTextFromOption(option)))
                .collect(Collectors.toList());
        for (WebElement selectedOption : elementsToBeSelected) {
            WebElement elementToClickFromOption = getElementToClickFromOption(selectedOption);
            waitForElementToBeClickableAndClick(elementToClickFromOption);
        }
        if (ddlShouldBeClosedManually) {
            waitForElementToBeClickableAndClick(ddlButtonFromContainer);
        }
    }

    @Override
    public List<String> getAvailableOptionsFromDDL(WebElement ddl) {
        waitForElementToBeClickableAndClick(ddl);
        List<WebElement> ddlOptions = findOptionRowsFromDDL(ddl);
        List<String> optionsTextList = ddlOptions.stream()
                .map(this::getTextFromOption)
                .collect(Collectors.toList());
        waitForElementToBeClickableAndClick(ddl);
        return optionsTextList;
    }


    @Override
    public String getSelectedItem(WebElement ddlComponent) {
        return getDDLSelectedText(ddlComponent);
    }

    protected abstract String getTextFromOption(WebElement option);

    protected abstract WebElement getElementToClickFromOption(WebElement option);

    protected abstract WebElement getDDLButtonFromContainer(WebElement ddlContainer);

    protected abstract WebElement getDDLElementFromContainer(WebElement ddlContainer);

    protected abstract List<WebElement> findOptionRowsFromDDL(WebElement ddl);

    protected abstract boolean checkIfOptionIsSelected(WebElement option);

    protected abstract String getDDLSelectedText(WebElement ddlComponent);

}
