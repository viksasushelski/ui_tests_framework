package Features.DropDownLists;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface DDLService {

    void chooseItemFromDDL(WebElement ddlContainer, Enum<?>... optionEnum);

    List<String> getAvailableOptionsFromDDL(WebElement ddl);

    String getSelectedItem(WebElement ddlComponent);

}
