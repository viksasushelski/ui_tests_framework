package Features.Tables;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface TableReader<T> {

    List<T> readTable(WebElement table);
}
