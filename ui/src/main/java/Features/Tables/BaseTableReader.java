package Features.Tables;

import UiFunctions.SeleniumFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class BaseTableReader<T> extends SeleniumFunctions implements TableReader {

    private By tableLocatorFromComponent;
    private By tableBodyLocatorFromTable;
    private By tableRowsLocatorFromTableBody;

    public BaseTableReader(WebDriver driver,  By tableLocatorFromComponent, By tableBodyLocatorFromTable, By tableRowsLocatorFromTableBody, int maxWaitingTimeForElements) {
        super(driver, maxWaitingTimeForElements);
        this.tableLocatorFromComponent = tableLocatorFromComponent;
        this.tableBodyLocatorFromTable = tableBodyLocatorFromTable;
        this.tableRowsLocatorFromTableBody = tableRowsLocatorFromTableBody;
    }

    public final List<T> readTable(WebElement tableComponent) {
        WebElement table = tableComponent;
        if (tableLocatorFromComponent != null) {
            table = findTable(tableComponent, tableLocatorFromComponent);
        }
        List<T> tableRowList = new ArrayList<>();
        WebElement tableBody = table;
        if (tableBodyLocatorFromTable != null) {
            tableBody = findTableBody(table, tableBodyLocatorFromTable);
        }
        List<WebElement> tableRows = findTableRows(tableBody, tableRowsLocatorFromTableBody);
        for (WebElement tableRow : tableRows) {
            tableRowList.add(readTableRow(tableRow));
        }
        return tableRowList;
    }

    public WebElement findTable(WebElement tableComponent, By by) {
        return waitAndFindElement(tableComponent, by);
    }

    public WebElement findTableBody(WebElement table, By by) {
        return waitAndFindElement(table, by);
    }

    public abstract T readTableRow(WebElement tableRow);

    public String readTableCellText(WebElement tableColumn, By by) {
        return waitAndFindElement(tableColumn, by).getText();
    }

    public String readTableCellText(WebElement tableColumn) {
        return tableColumn.getText();
    }

    public WebElement readTableCellWebElement(WebElement tableColumn, By by) {
        return waitAndFindElement(tableColumn, by);
    }

    public WebElement readTableCellWebElement(WebElement tableColumn) {
        return tableColumn;
    }

    public Optional<WebElement> findTableColumn(WebElement tableRow, By by) {
        try {
            return Optional.of(tableRow.findElement(by));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public List<WebElement> findTableRows(WebElement tableBody, By by) {
        return waitAndFindElements(tableBody, by);
    }

}
