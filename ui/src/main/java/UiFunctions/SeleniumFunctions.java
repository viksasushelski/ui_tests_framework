package UiFunctions;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Optional;

@Getter
public class SeleniumFunctions {
    private WebDriver driver;
    private WebDriverWait wait;
    private AjaxElementLocatorFactory factory;
    private Actions actions;
    private JavascriptExecutor javascriptExecutor;

    public SeleniumFunctions(WebDriver driver, int maxWaitingTimeForElements) {
        this.driver = driver;
        this.factory = new AjaxElementLocatorFactory(driver, maxWaitingTimeForElements);
        PageFactory.initElements(factory, this);
        wait = new WebDriverWait(driver, maxWaitingTimeForElements);
        actions = new Actions(driver);
        javascriptExecutor = (JavascriptExecutor) driver;
    }

    protected void clearAndSendKeys(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element));
        moveToElement(element);
        element.clear();
        element.sendKeys(text);
    }

    protected void waitUntilElementIsRemoved(By by, int seconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, seconds);
        try {
            webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        } catch (Exception e) {
        }
    }

    protected void waitUntilElementIsDisplayed(By by, int seconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, seconds);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
        }
    }

    protected Long waitForElementToBeShownAndRemoved(By by, int secondsToBeShown, int secondsToBeRemoved) {
        //long start = System.currentTimeMillis();
        waitUntilElementIsDisplayed(by, secondsToBeShown);
        waitUntilElementIsRemoved(by, secondsToBeRemoved);
        return System.currentTimeMillis();
    }

    public void moveToElement(WebElement element) {
        actions.moveToElement(element);
        actions.perform();
    }


    protected void removeAttributeFromElement(WebElement elem, String attributeName) {
        String script = "arguments[0].removeAttribute('" + attributeName + "','" + attributeName + "')";
        javascriptExecutor.executeScript(script, elem);
    }

    protected void removeClassNameFromClassAttribute(WebElement elem, String className) {
        String script = "arguments[0].classList.remove('" + className + "','" + className + "')";
        javascriptExecutor.executeScript(script, elem);
    }

    public void moveToTopOfPage() {
        javascriptExecutor.executeScript("window.scrollTo(0, 0)");
    }

    protected void waitForElementToBeClickableAndClick(WebElement elem) {
        moveToElement(elem);
        wait.until(ExpectedConditions.elementToBeClickable(elem));
        elem.click();
    }

    protected WebElement waitAndFindElement(WebElement root, By byLocator) {
        wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(root, byLocator));
        return root.findElement(byLocator);
    }

    protected WebElement waitAndFindElementFromRoot(By byLocator) {
        wait.until(ExpectedConditions.presenceOfElementLocated(byLocator));
        return driver.findElement(byLocator);
    }

    protected WebElement waitAndFindElementFromRoot(By byLocator, int secondsToWait) {
        WebDriverWait methodWait = new WebDriverWait(driver,secondsToWait);
        methodWait.until(ExpectedConditions.presenceOfElementLocated(byLocator));
        return driver.findElement(byLocator);
    }


    protected List<WebElement> waitAndFindElements(WebElement root, By byLocator) {
        wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(root, byLocator));
        return root.findElements(byLocator);
    }

    protected List<WebElement> waitAndFindElementsIfPresent(WebElement root, By byLocator) {
        wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(root, byLocator));
        return root.findElements(byLocator);
    }

    protected boolean checkIfElementIsPresent(WebElement root, By locator) {
        try {
            root.findElement(locator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected Boolean checkIfElementIsDisplayed(WebElement webElement) {
        try {
            webElement.isDisplayed();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected Boolean checkIfElementIsDisplayed(WebElement from, By by) {
        try {
            return from.findElement(by).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean checkIfElementIsPresentFromRoot(By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected Optional<WebElement> checkAndGetIfElementIsPresent(WebElement root, By locator) {
        try {
            return Optional.of(root.findElement(locator));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public boolean checkIfElementIsEnabled(WebElement elem) {
        try {
            String className = elem.getAttribute("disabled");
            return className == null;
        } catch (Exception e) {
            return true;
        }
    }

    protected void scrollToRightOfPage() {
        javascriptExecutor
                .executeScript("window.scrollBy(2000,0)");
    }

    protected void scrollToBottomOfPage() {
        javascriptExecutor
                .executeScript("window.scrollTo(0, 500)");
    }

    protected void clickElementUsingJavaScriptExecutor(WebElement element) {
        javascriptExecutor.executeScript("arguments[0].click();", element);
    }

    protected WebElement getParentElement(WebElement child) {
        return waitAndFindElement(child, By.xpath(".."));
    }

    protected Optional<WebElement> checkAndGetIfElementIsPresentFromRoot(By locator) {
        try {
            return Optional.of(driver.findElement(locator));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
