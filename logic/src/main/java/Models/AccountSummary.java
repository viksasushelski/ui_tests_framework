package Models;

import Utils.CompareDifferences;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.openqa.selenium.WebElement;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AccountSummary implements CompareDifferences {

    private String accountName;
    private String balance;
    private String creditCardNumber;

    @JsonIgnore
    private WebElement accountLink;

    @Override
    public String toString() {
        return "AccountSummary{" +
                "accountName='" + accountName + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountSummary that = (AccountSummary) o;

        if (!accountName.equals(that.accountName)) return false;
        if (!balance.equals(that.balance)) return false;
        return creditCardNumber != null ? creditCardNumber.equals(that.creditCardNumber) : that.creditCardNumber == null;
    }

    @Override
    public int hashCode() {
        int result = accountName.hashCode();
        result = 31 * result + balance.hashCode();
        result = 31 * result + (creditCardNumber != null ? creditCardNumber.hashCode() : 0);
        return result;
    }
}
