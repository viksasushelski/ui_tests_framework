package Pages.Login;

import Log.Log;
import Pages.AccountSummary.AccountSummaryPage;
import Pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Optional;

public class LoginPage extends BasePage implements LoginPageAssertions {

    public static final String ALERT_ERROR_CLASS_LOCATOR = "alert-error";
    public static final String INDICATOR_ELEMENT_ID_LOCATOR = "login_form";
    @FindBy(id = "user_login")
    private WebElement inputUserName;

    @FindBy(id = "user_password")
    private WebElement inputPassword;

    @FindBy(name = "submit")
    private WebElement btnSubmit;


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new LoginPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindElementFromRoot(By.id(INDICATOR_ELEMENT_ID_LOCATOR), 3);
    }

    @Override
    public LoginPage getLoginPage() {
        return this;
    }

    public LoginPage insertUserName(String userName) {
        clearAndSendKeys(inputUserName, userName);
        Log.stepsToReproduce("User has inserted username with value: " + userName);
        return new LoginPage(getDriver());
    }

    public LoginPage insertPassword(String password) {
        clearAndSendKeys(inputPassword, password);
        Log.stepsToReproduce("User has inserted password with value: " + password);
        return new LoginPage(getDriver());
    }

    public LoginPage clickSubmit() {
        waitForElementToBeClickableAndClick(btnSubmit);
        Log.stepsToReproduce("User has clicked the submit button");
        return new LoginPage(getDriver());
    }

    public AccountSummaryPage loginUsingCredentials(String userName, String password) {
        insertUserName(userName)
                .insertPassword(password)
                .clickSubmit();
        return new AccountSummaryPage(this.getDriver());
    }

    public Optional<String> getValidationMessage() {
        Optional<WebElement> alertDiv = checkAndGetIfElementIsPresentFromRoot(By.className(ALERT_ERROR_CLASS_LOCATOR));
        return alertDiv.isPresent()
                ? Optional.of(alertDiv.get().getText())
                : Optional.empty();
    }
}
