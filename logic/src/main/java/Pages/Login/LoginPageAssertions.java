package Pages.Login;

import Models.AssertionResult;

import java.util.Optional;

public interface LoginPageAssertions {

    LoginPage getLoginPage();

    default AssertionResult assertValidationMessageIsDisplayed() {
        Optional<String> validationMessage = getLoginPage().getValidationMessage();
        return AssertionResult.builder()
                .passed(validationMessage.isPresent())
                .errorMessage("The validation is not present")
                .build();
    }


    default AssertionResult assertValidationMessageIsCorrect(String expectedMessage) {
        Optional<String> validationMessageOptional = getLoginPage().getValidationMessage();

        if (!validationMessageOptional.isPresent()) {
            return AssertionResult.builder()
                    .passed(false)
                    .errorMessage("The validation message is not present")
                    .build();
        }

        String validationMessage = validationMessageOptional.get();
        return AssertionResult.builder()
                .passed(validationMessage.equals(expectedMessage))
                .errorMessage("The validation message is not correct.\nExpected: " + expectedMessage + "\nActual: " + validationMessage + "\n")
                .build();

    }
}
