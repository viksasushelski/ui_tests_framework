package Pages.AccountActivity;

import Components.ShowTransactionsComponent;
import Data.Enums.AccountActivityType;
import Data.Enums.AccountTypeEnum;
import Pages.BasePage;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class AccountActivityPage extends BasePage {

    @FindBy(linkText = "Show Transactions")
    private WebElement showTransactionsLink;

    private ShowTransactionsComponent showTransactionsComponent;

    public AccountActivityPage(WebDriver driver) {
        super(driver);
    }

    public AccountActivityPage(WebDriver driver, AccountActivityType accountActivityType) {
        super(driver);

        if (accountActivityType == AccountActivityType.SHOW_TRANSACTIONS) {
            showTransactionsComponent = new ShowTransactionsComponent(driver);
        }
        if (accountActivityType == AccountActivityType.FIND_TRANSACTIONS) {

        }
    }

    public AccountActivityPage(AccountActivityPage other, AccountActivityType accountActivityType) {
        super(other.getDriver());

        if (accountActivityType == AccountActivityType.SHOW_TRANSACTIONS) {
            showTransactionsComponent = other.getShowTransactionsComponent();
        }
        if (accountActivityType == AccountActivityType.FIND_TRANSACTIONS) {

        }
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new AccountActivityPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindElementFromRoot(By.id("show_transactions_form"), 5);
    }

    public AccountActivityPage navigateToShowTransactionTab() {
        waitForElementToBeClickableAndClick(showTransactionsLink);
        return new AccountActivityPage(getDriver(), AccountActivityType.SHOW_TRANSACTIONS);
    }

    public AccountActivityPage chooseAccount(AccountTypeEnum accountTypeEnum) {
        showTransactionsComponent.chooseAccount(accountTypeEnum);
        return new AccountActivityPage(getDriver(), AccountActivityType.SHOW_TRANSACTIONS);
    }

    public AccountActivityPage readTransactionsTable() {
        showTransactionsComponent.readTransactions();
        return new AccountActivityPage(this, AccountActivityType.SHOW_TRANSACTIONS);
    }
}
