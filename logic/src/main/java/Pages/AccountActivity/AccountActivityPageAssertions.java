package Pages.AccountActivity;

import Components.Tables.AccountSummaryTableComponent;
import Components.Tables.ShowTransactionsTableComponent;
import Data.Enums.AccountModelTypeEnum;
import Models.AccountSummary;
import Models.AssertionResult;
import Models.Transaction;
import Pages.AccountSummary.AccountSummaryPage;
import Utils.TestAssertionUtil;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.util.List;

public interface AccountActivityPageAssertions {

    AccountActivityPage getAccountActivityPage();

    default AssertionResult assertTableContent(List<Transaction> expectedTransactionList) {
        ShowTransactionsTableComponent transactionsTableComponent = getAccountActivityPage()
                .getShowTransactionsComponent().getTransactionsTableComponent();
        List<Transaction> actualTransactionList = (List<Transaction>) transactionsTableComponent.getListOfEntries();
        return TestAssertionUtil.checkIfListsAreEqual(actualTransactionList, expectedTransactionList);
    }

}
