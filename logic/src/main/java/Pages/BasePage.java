package Pages;

import Log.Log;
import UiFunctions.SeleniumFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

public abstract class BasePage extends SeleniumFunctions {

    public static final int MAX_WAITING_TIME_FOR_ELEMENTS = 10;

    public BasePage(WebDriver driver) {
        super(driver, MAX_WAITING_TIME_FOR_ELEMENTS);
        try {
            getPageIsLoadedIndicatorElement();
        } catch (Exception e) {
            Log.error("The indicator elements wasn't found");
        }
        writeDriversLogs();
    }

    public abstract BasePage newInstance(WebDriver driver);

    public abstract WebElement getPageIsLoadedIndicatorElement();

    public BasePage navigateTo(String url) {
        getDriver().get(url);
        Log.stepsToReproduce("User has navigated to: " + url);
        return newInstance(getDriver());
    }

    protected void writeDriversLogs() {
        LogEntries browser = getDriver().manage().logs().get(LogType.BROWSER);
        for (LogEntry logEntry : browser.getAll()) {
            Log.writeConsoleLog(logEntry.getMessage());
        }
    }
}
