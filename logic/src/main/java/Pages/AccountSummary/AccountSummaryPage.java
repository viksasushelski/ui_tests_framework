package Pages.AccountSummary;

import Components.Tables.AccountSummaryTableComponent;
import Data.Enums.AccountModelTypeEnum;
import Features.Tables.AccountSummaryTable;
import Features.Tables.TableReader;
import Models.AccountSummary;
import Pages.AccountActivity.AccountActivityPage;
import Pages.BasePage;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@Getter
public class AccountSummaryPage extends BasePage implements AccountSummaryPageAssertions {

    @FindBy(className = "table")
    private List<WebElement> tablesContainers;

    private AccountSummaryTableComponent cashAccountSummaryTableComponent;
    private AccountSummaryTableComponent investmentAccountSummaryTableComponent;
    private AccountSummaryTableComponent creditAccountSummaryTableComponent;
    private AccountSummaryTableComponent loanAccountSummaryTableComponent;
    private TableReader tableReader;
    private Map<AccountModelTypeEnum, AccountSummaryTableComponent> accountModelTypeEnumAccountSummaryTableComponentMap;
    private Map<AccountModelTypeEnum, Supplier<AccountSummaryPage>> accountModelTypeEnumReadFunctionMap;

    {
        populateReadDataMap();
    }


    public AccountSummaryPage(WebDriver driver) {
        super(driver);
        tableReader = new AccountSummaryTable(driver);
        cashAccountSummaryTableComponent = new AccountSummaryTableComponent(tablesContainers.get(0), tableReader);
        investmentAccountSummaryTableComponent = new AccountSummaryTableComponent(tablesContainers.get(1), tableReader);
        creditAccountSummaryTableComponent = new AccountSummaryTableComponent(tablesContainers.get(2), tableReader);
        loanAccountSummaryTableComponent = new AccountSummaryTableComponent(tablesContainers.get(3), tableReader);
        populateComponentMap();
    }

    public AccountSummaryPage(AccountSummaryPage other) {
        super(other.getDriver());
        tableReader = other.getTableReader();
        cashAccountSummaryTableComponent = other.getCashAccountSummaryTableComponent();
        investmentAccountSummaryTableComponent = other.getInvestmentAccountSummaryTableComponent();
        creditAccountSummaryTableComponent = other.getCreditAccountSummaryTableComponent();
        loanAccountSummaryTableComponent = other.getLoanAccountSummaryTableComponent();
        this.accountModelTypeEnumAccountSummaryTableComponentMap = other.accountModelTypeEnumAccountSummaryTableComponentMap;
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new AccountSummaryPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindElementFromRoot(By.className("board"), 2);
    }

    public AccountSummaryPage readCashAccounts() {
        cashAccountSummaryTableComponent.fillListOfEntires();
        return this;
    }

    public AccountSummaryPage readInvestmentAccounts() {
        investmentAccountSummaryTableComponent.fillListOfEntires();
        return this;
    }

    public AccountSummaryPage readCreditAccounts() {
        creditAccountSummaryTableComponent.fillListOfEntires();
        return this;
    }

    public AccountSummaryPage readLoanAccounts() {
        loanAccountSummaryTableComponent.fillListOfEntires();
        return this;
    }

    public Optional<AccountSummary> findAccount(AccountModelTypeEnum accountType, String accountName) {
        return accountModelTypeEnumAccountSummaryTableComponentMap.get(accountType).findEntryByValue(accountName);
    }

    public AccountActivityPage openAccountActivityPage(AccountModelTypeEnum accountType, String accountName) {
        accountModelTypeEnumAccountSummaryTableComponentMap.get(accountType).openAccountActivityPage(accountName);
        return new AccountActivityPage(this.getDriver());
    }

    private void populateComponentMap() {
        accountModelTypeEnumAccountSummaryTableComponentMap = new HashMap<>();
        accountModelTypeEnumAccountSummaryTableComponentMap.put(AccountModelTypeEnum.CASH, cashAccountSummaryTableComponent);
        accountModelTypeEnumAccountSummaryTableComponentMap.put(AccountModelTypeEnum.CREDIT, creditAccountSummaryTableComponent);
        accountModelTypeEnumAccountSummaryTableComponentMap.put(AccountModelTypeEnum.INVESTMENT, investmentAccountSummaryTableComponent);
        accountModelTypeEnumAccountSummaryTableComponentMap.put(AccountModelTypeEnum.LOAN, loanAccountSummaryTableComponent);
    }

    private void populateReadDataMap() {
        accountModelTypeEnumReadFunctionMap = new HashMap<>();
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.CASH, this::readCashAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.CREDIT, this::readCreditAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.INVESTMENT, this::readInvestmentAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.LOAN, this::readLoanAccounts);
    }

    @Override
    public AccountSummaryPage getAccountSummaryPage() {
        return this;
    }
}
