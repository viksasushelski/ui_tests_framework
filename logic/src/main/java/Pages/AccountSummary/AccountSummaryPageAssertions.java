package Pages.AccountSummary;

import Components.Tables.AccountSummaryTableComponent;
import Data.Enums.AccountModelTypeEnum;
import Data.Enums.AccountTypeEnum;
import Json.JsonParser;
import Log.Log;
import Models.AccountSummary;
import Models.AssertionResult;
import Utils.TestAssertionUtil;
import org.openqa.selenium.WebElement;

import java.util.List;

public interface AccountSummaryPageAssertions {

    AccountSummaryPage getAccountSummaryPage();

    default AssertionResult assertTableContent(AccountModelTypeEnum accountModelTypeEnum, List<AccountSummary> expectedAccountList) {
        AccountSummaryTableComponent accountSummaryTableComponent = getAccountSummaryPage()
                .getAccountModelTypeEnumAccountSummaryTableComponentMap().get(accountModelTypeEnum);
        List<AccountSummary> actualAccountList = (List<AccountSummary>) accountSummaryTableComponent.getListOfEntries();
        Log.assertion("The values for account with type: " + accountModelTypeEnum.toString() + " should be: " + JsonParser.getJsonStringFromListOfObjects(expectedAccountList).toString());
        return TestAssertionUtil.checkIfListsAreEqual(actualAccountList, expectedAccountList, WebElement.class);
    }

}
