package Features.DropDownLists;

import Data.Constants.ApplicationConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SimpleDropDownList extends BaseDDL {

    public SimpleDropDownList(WebDriver driver) {
        super(driver, ApplicationConstants.MAX_DDL_READER_WAITING_TIME, false, false);
    }

    @Override
    protected String getTextFromOption(WebElement option) {
        return option.getText();
    }

    @Override
    protected WebElement getElementToClickFromOption(WebElement option) {
        return option;
    }

    @Override
    protected WebElement getDDLButtonFromContainer(WebElement ddlContainer) {
        return waitAndFindElement(ddlContainer, By.tagName("select"));
    }

    @Override
    protected WebElement getDDLElementFromContainer(WebElement ddlContainer) {
        return waitAndFindElement(ddlContainer, By.tagName("select"));
    }

    @Override
    protected List<WebElement> findOptionRowsFromDDL(WebElement ddl) {
        return waitAndFindElements(ddl,By.xpath("option"));
    }

    @Override
    protected boolean checkIfOptionIsSelected(WebElement option) {
        return false;
    }

    @Override
    protected String getDDLSelectedText(WebElement ddlComponent) {
        return null;
    }
}
