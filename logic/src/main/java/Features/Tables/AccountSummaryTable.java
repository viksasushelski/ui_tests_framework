package Features.Tables;

import Data.Constants.ApplicationConstants;
import Models.AccountSummary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Optional;

public class AccountSummaryTable extends BaseTableReader<AccountSummary> {

    public AccountSummaryTable(WebDriver driver) {
        //todo remove seconds
        super(driver, null, By.tagName("tbody"), By.xpath("tr"), ApplicationConstants.MAX_TABLE_READER_WAITING_TIME);
    }

    @Override
    public AccountSummary readTableRow(WebElement tableRow) {
        Optional<WebElement> accountColumn = findTableColumn(tableRow, By.cssSelector("td:nth-child(1)"));
        Optional<WebElement> creditCardColumn = findTableColumn(tableRow, By.cssSelector("td:nth-child(2)"));
        Optional<WebElement> balanceColumn = findTableColumn(tableRow, By.cssSelector("td:nth-child(3)"));

        return AccountSummary.builder()
                .accountLink(accountColumn.isPresent() ? waitAndFindElement(accountColumn.get(), By.tagName("a")) : null)
                .accountName(accountColumn.isPresent() ? waitAndFindElement(accountColumn.get(), By.tagName("a")).getText() : null)
                .creditCardNumber(creditCardColumn.isPresent() ? creditCardColumn.get().getText() : null)
                .balance(balanceColumn.isPresent() ? balanceColumn.get().getText() : null)
                .build();
    }

}
