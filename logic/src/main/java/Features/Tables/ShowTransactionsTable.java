package Features.Tables;

import Data.Constants.ApplicationConstants;
import Models.Transaction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Optional;

public class ShowTransactionsTable extends BaseTableReader<Transaction> {

    public ShowTransactionsTable(WebDriver driver) {
        super(driver, By.tagName("table"), By.tagName("tbody"), By.xpath("tr"), ApplicationConstants.MAX_TABLE_READER_WAITING_TIME);
    }

    @Override
    public Transaction readTableRow(WebElement tableRow) {
        Optional<WebElement> dateOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(1)"));
        Optional<WebElement> descriptionOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(2)"));
        Optional<WebElement> depositOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(3)"));
        Optional<WebElement> withdrawalOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(4)"));
        return Transaction.builder()
                .date(dateOptional.isPresent() ? readTableCellText(dateOptional.get()) : null)
                .description(descriptionOptional.isPresent() ? readTableCellText(descriptionOptional.get()) : null)
                .deposit(depositOptional.isPresent() ? readTableCellText(depositOptional.get()) : null)
                .withdrawal(withdrawalOptional.isPresent() ? readTableCellText(withdrawalOptional.get()) : null)
                .build();
    }
}
