package Data.Constants;

public class ApplicationConstants {

    public final static int MAX_TABLE_READER_WAITING_TIME = 5;
    public final static int MAX_COMPONENT_WAITING_TIME = 3;
    public final static int MAX_DDL_READER_WAITING_TIME = 5;

}
