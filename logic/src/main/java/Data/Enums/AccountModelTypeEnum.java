package Data.Enums;

public enum AccountModelTypeEnum {

    CASH,
    INVESTMENT,
    CREDIT,
    LOAN;
}
