package Data.Enums;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum AccountTypeEnum {
    SAVINGS("Savings"),
    CHECKINGS("Checking"),
    LOAN("Loan"),
    CREDIT_CARD("Credit Card"),
    BROKERAGE("Brokerage");

    private final String text;

    AccountTypeEnum(final String text) {
        this.text = text;
    }

    private static Map<String, AccountTypeEnum> values = populateMap();

    private static Map<String, AccountTypeEnum> populateMap() {
        Map<String, AccountTypeEnum> valuesMap = new HashMap<>();
        AccountTypeEnum[] values = values();
        Stream.of(values).forEach(x -> valuesMap.put(x.text, x));
        return valuesMap;
    }

    @Override
    public String toString() {
        return text;
    }

    public static AccountTypeEnum parse(String text) {
        if (!values.containsKey(text)) {
            throw new IllegalArgumentException("text");
        }

        return values.get(text);
    }

}
