package Data.Enums;

public enum AccountActivityType {

    SHOW_TRANSACTIONS,
    FIND_TRANSACTIONS;
}
