package Exceptions;

import java.util.function.Supplier;

/**
 * Created by vsushelski on 8/21/2017.
 */
public class TestFlowException extends RuntimeException implements Supplier<TestFlowException> {
    public TestFlowException() {
    }

    public TestFlowException(String message) {
        super(message);
    }

    @Override
    public TestFlowException get() {
        return null;
    }
}
