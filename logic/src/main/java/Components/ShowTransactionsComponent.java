package Components;

import Components.DropDownLists.SimpleDDLComponent;
import Components.Tables.ShowTransactionsTableComponent;
import Data.Constants.ApplicationConstants;
import Data.Enums.AccountTypeEnum;
import Features.DropDownLists.SimpleDropDownList;
import Features.Tables.ShowTransactionsTable;
import UiFunctions.SeleniumFunctions;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


@Getter
public class ShowTransactionsComponent extends SeleniumFunctions {

    @FindBy(id = "show_transactions_form")
    private WebElement ddlContainer;

    @FindBy(id = "all_transactions_for_account")
    private WebElement tableContainer;

    private SimpleDDLComponent ddlComponent;
    private ShowTransactionsTableComponent transactionsTableComponent;

    private SimpleDropDownList simpleDropDownListService;
    private ShowTransactionsTable showTransactionsTableService;


    public ShowTransactionsComponent(WebDriver driver) {
        super(driver, ApplicationConstants.MAX_COMPONENT_WAITING_TIME);
        simpleDropDownListService = new SimpleDropDownList(driver);
        showTransactionsTableService = new ShowTransactionsTable(driver);
        ddlComponent = new SimpleDDLComponent(ddlContainer, simpleDropDownListService);
        transactionsTableComponent = new ShowTransactionsTableComponent(tableContainer, showTransactionsTableService);
    }

    public ShowTransactionsComponent(ShowTransactionsComponent other) {
        super(other.getDriver(), ApplicationConstants.MAX_COMPONENT_WAITING_TIME);
        ddlComponent = other.getDdlComponent();
        transactionsTableComponent = other.getTransactionsTableComponent();
    }

    public void readTransactions() {
        transactionsTableComponent.fillListOfEntires();
    }

    public void chooseAccount(AccountTypeEnum accountTypeEnum) {
        ddlComponent.chooseFromDDL(accountTypeEnum);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
