package Components.Tables;

import Exceptions.TestFlowException;
import Features.Tables.TableReader;
import Models.AccountSummary;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;

public class AccountSummaryTableComponent extends BaseTableComponent {

    public AccountSummaryTableComponent(WebElement tableComponent, TableReader tableReader) {
        super(tableComponent, tableReader);
    }

    public Optional<AccountSummary> findEntryByValue(String name) {
        List<AccountSummary> listOfEntries = (List<AccountSummary>) getListOfEntries();
        if (listOfEntries.isEmpty()) {
            throw new TestFlowException("The list of accounts is empty");
        }
        return listOfEntries.stream().filter(accountSummary -> accountSummary.getAccountName().equals(name))
                .findFirst();
    }

    public void openAccountActivityPage(String name) {
        Optional<AccountSummary> accountOptional = findEntryByValue(name);
        if (!accountOptional.isPresent()) {
            throw new TestFlowException("There is no account with name: " + name);
        }
        AccountSummary accountSummary = accountOptional.get();
        accountSummary.getAccountLink().click();
    }
}
