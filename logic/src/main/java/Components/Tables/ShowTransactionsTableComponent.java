package Components.Tables;

import Exceptions.TestFlowException;
import Features.Tables.TableReader;
import Models.AccountSummary;
import Models.Transaction;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;

public class ShowTransactionsTableComponent extends BaseTableComponent {

    public ShowTransactionsTableComponent(WebElement tableComponent, TableReader tableReader) {
        super(tableComponent, tableReader);
    }

    public Optional<Transaction> findEntryByValue(String description) {
        List<Transaction> listOfEntries = (List<Transaction>) getListOfEntries();
        if (listOfEntries.isEmpty()) {
            throw new TestFlowException("The list of transaction is empty");
        }
        return listOfEntries.stream().filter(transaction -> transaction.getDescription().equals(description))
                .findFirst();
    }

}
