package Components.DropDownLists;

import Features.DropDownLists.DDLService;
import org.openqa.selenium.WebElement;

public class SimpleDDLComponent extends BaseDDLComponent{

    public SimpleDDLComponent(WebElement ddlComponent, DDLService ddlService) {
        super(ddlComponent, ddlService);
    }
}
